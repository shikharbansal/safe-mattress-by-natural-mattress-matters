# What is Mattress Off-Gassing?

Mainly expressed, off gassing is the dissipation of volatile chemicals. Polyurethane froth and fire retardants found in non-natural mattresses off-gas and break down after some time. The average mattress is absorbed in a peculiar mix of flame retardant chemicals, formaldehyde, and other lethal pastes, stains, and coatings, and characteristically produced using off-gassing segments like petroleum based polyurethane froth. These chemicals can discharge dangerous cancer-causing gasses.

### Could You Smell Off-Gassing?

A portion of the deadliest off-gassing is due to the parasite and microscopic organisms expanding fire retardants and discharging lethal repercussions. This can occur in more established mattresses which no longer have any scent. Try not to be tricked into thinking a mattress is totally protected in light of the fact that it doesn't smell.

### Hypersensitive Reactions to Mattress Off-Gassing

Numerous grown-ups have reported migraines, sickness, and peculiar unfavorably susceptible responses while thinking about new non-organic mattresses. With each report of this nature, it is essential to note where the mattress was obtained, and also the materials and chemicals utilized as a part of the assembling of the mattress. On the off chance that you are having bizarre or antagonistic impacts from a new mattress, it is possible that off-gassing is the cause. We recommend  purchasing a [Safe Mattress by Natural Mattress Matters](https://www.naturalmattressmatters.com/safe-mattress-buying-guide/).